function Train() {
  Container.call(this);
  var self = this;

  placeCabin();

  placeChassis();

  placeEngine();

  placeFront();

  function placeCabin() {
    var cabin = new TrainCabin();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0.75, 2.35]);

    cabin.setTransformationMatrix(mat);

    self.addChild(cabin);
  }

  function placeChassis() {
    var chassis = new TrainChassis();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0, 0]);

    chassis.setTransformationMatrix(mat);

    self.addChild(chassis);
  }

  function placeEngine() {
    var engine = new Cylinder(6, 32, new TrainMaterial());

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, -1.1, 2.35]);
    mat4.scale(mat, mat, [2.5, 1.5, 2.5]);

    engine.setTransformationMatrix(mat);

    self.addChild(engine);
  }

  function placeFront() {
    var front = new TrainFront();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, -2.1, 2.35]);

    front.setTransformationMatrix(mat);

    self.addChild(front);
  }
}

inheritPrototype(Train, Container);

function TrainFront() {
  Container.call(this);
  var self = this;

  placeEngineCover();
  placeWheelCovers();
  placeChimney();

  function placeEngineCover() {
    var cover = new Cylinder(6, 32, new TrainMaterial());
    cover.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.scale(mat, mat, [3.0, 0.5, 3.0]);

    cover.setTransformationMatrix(mat);

    self.addChild(cover);

    var point = new Cylinder(6, 16, new TrainMaterial());
    point.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, -0.25, 0]);
    mat4.scale(mat, mat, [0.6, 0.5, 0.6]);

    point.setTransformationMatrix(mat);

    self.addChild(point);
  }

  function placeWheelCovers() {
    var smallCoverLeft = new Cylinder(6, 16, new TrainMaterial());
    smallCoverLeft.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.rotateY(mat, mat, Math.PI/6);
    mat4.translate(mat, mat, [0, 0, -2.0]);
    mat4.scale(mat, mat, [1.0, 0.5, 1.0]);

    smallCoverLeft.setTransformationMatrix(mat);

    self.addChild(smallCoverLeft);

    var smallCoverRight = new Cylinder(6, 16, new TrainMaterial());
    smallCoverRight.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.rotateY(mat, mat, -Math.PI/6);
    mat4.translate(mat, mat, [0, 0, -2.0]);
    mat4.scale(mat, mat, [1.0, 0.5, 1.0]);

    smallCoverRight.setTransformationMatrix(mat);

    self.addChild(smallCoverRight);
  }

  function placeChimney() {
    var chimney = new Cylinder(6, 16, new TrainMaterial());
    chimney.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0, 2.25]);
    mat4.rotateX(mat, mat, Math.PI/2);
    mat4.scale(mat, mat, [0.5, 1.5, 0.5]);

    chimney.setTransformationMatrix(mat);

    self.addChild(chimney);
  }
}

inheritPrototype(TrainFront, Container);

function TrainChassis() {
  Container.call(this);
  var self = this;

  this.WHEEL_DIAMETER = 1.0;
  this.FLOOR_HEIGHT = 0.1;

  placeFloor();
  placeWheel([0.9, 0.5, this.WHEEL_DIAMETER/2]);
  placeWheel([-0.9, 0.5, this.WHEEL_DIAMETER/2]);
  placeWheel([0.9, -1.25, this.WHEEL_DIAMETER/2]);
  placeWheel([-0.9, -1.25, this.WHEEL_DIAMETER/2]);

  placeMovingBar();

  function placeWheel(position) {
    var wheel = new Cylinder(6, 16, new TrainMaterial());
    wheel.material = new WheelMaterial();

    var wheelMat = mat4.create();
    mat4.translate(wheelMat, wheelMat, position);
    mat4.rotateZ(wheelMat, wheelMat, Math.PI/2);
    mat4.scale(wheelMat, wheelMat, [self.WHEEL_DIAMETER, 0.5, self.WHEEL_DIAMETER]);

    wheel.setTransformationMatrix(wheelMat);

    self.addChild(wheel);
  }

  function placeFloor() {
    var floor = new Box();
    floor.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0, self.WHEEL_DIAMETER + self.FLOOR_HEIGHT/2]);
    mat4.scale(mat, mat, [2.5, 3.7, 0.1]);

    floor.setTransformationMatrix(mat);

    self.addChild(floor);
  }

  function placeMovingBar() {
    self.bars = [];

    self.bars[0] = {};
    self.bars[0].segments = [];
    self.bars[0].nodes = [];
    self.bars[0].nodes.push([1.2, -1.25, 0.1]);
    self.bars[0].nodes.push([1.25, 0.5, 0.1]);
    self.bars[0].nodes.push([1.3, -0.5, self.WHEEL_DIAMETER/2 - 0.1]);
    self.bars[0].nodes.push([1.3, -1.35 - self.WHEEL_DIAMETER/2, self.WHEEL_DIAMETER/2 - 0.1]);

    /* the other bar is a mirrored copy of the first one */
    self.bars[1] = {};
    self.bars[1].segments = [];
    self.bars[1].nodes = [];
    for(var i = 0; i < self.bars[0].nodes.length; i++) {
      var node = self.bars[0].nodes[i].slice(0)
      node[0] *= -1;
      self.bars[1].nodes.push(node);
    }

    /* set the materials and add the bars to the containing object */
    for(var i = 0; i < self.bars.length; i++){
      for(var j = 0; j < self.bars[i].nodes.length; j++){
        var node = new Cylinder(6, 16, new MovingBarMaterial());
        node.pos = self.bars[i].nodes[j];
        self.bars[i].nodes[j] = node;
        if(j < self.bars[i].nodes.length - 1)
          self.addChild(node);
      }
      for(var j = 0; j < self.bars[i].nodes.length - 1; j++){
        var bar = new Box();
        bar.material = new MovingBarMaterial();
        self.bars[i].segments.push(bar);
        self.addChild(bar);
      }
    }

    self.animate(0);
  }
}

inheritPrototype(TrainChassis, Container);

TrainChassis.prototype.animate = function(t) {
  for(var i = 0; i < this.bars.length; i++) {
    var nodes = this.bars[i].nodes;

    /* keeps constant the length of the second segment */
    var second_len = 1.0;

    nodes[0].pos[1] = Math.cos(t) * (this.WHEEL_DIAMETER/2 - 0.1) + -1.25;
    nodes[0].pos[2] = Math.sin(t) * (this.WHEEL_DIAMETER/2 - 0.1) + this.WHEEL_DIAMETER/2;

    nodes[1].pos[1] = Math.cos(t) * (this.WHEEL_DIAMETER/2 - 0.1) + 0.5;
    nodes[1].pos[2] = Math.sin(t) * (this.WHEEL_DIAMETER/2 - 0.1) + this.WHEEL_DIAMETER/2;

    nodes[2].pos[1] = nodes[1].pos[1] - Math.sqrt(Math.pow(second_len, 2) - Math.pow(nodes[1].pos[2] - nodes[2].pos[2], 2));

    for(var j = 0; j < this.bars[i].segments.length; j++) {
      var bar = this.bars[i].segments[j];
      var start = nodes[j].pos;
      var end = nodes[j+1].pos;
      var dir = vec3.subtract([], end, start);
      var length = vec3.length(dir);

      /* builds the matrix for the segment */
      var mat = mat4.create();
      mat4.translate(mat, mat, start);

      var forward = vec3.normalize([], dir);
      var right = [-1, 0, 0];
      var up = vec3.cross([], right, forward);
      mat4.multiply(mat, mat, [].concat(
        right, [0],
        forward, [0],
        up, [0],
        [0, 0, 0, 1]
      ));

      mat4.scale(mat, mat, [0.1, length, 0.2]);
      mat4.translate(mat, mat, [0, 0.5, 0]);

      bar.setTransformationMatrix(mat);

      /* builds the matrix for the node */
      var node = nodes[j];
      mat = mat4.create();
      mat4.translate(mat, mat, start);
      mat4.rotateZ(mat, mat, Math.PI/2);
      mat4.scale(mat, mat, [0.4, 0.1, 0.4]);
      node.setTransformationMatrix(mat);
    }
  }
}


function TrainCabin() {
  Container.call(this);
  var self = this;

  placeDriversBox();
  placeBackGuard();

  placeBar([1.20, 1.05, 2.125]);
  placeBar([1.20, -1.05, 2.125]);
  placeBar([-1.20, 1.05, 2.125]);
  placeBar([-1.20, -1.05, 2.125]);

  placeRoof();

  function placeDriversBox() {
    var cabin = new Box();
    cabin.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.scale(mat, mat, [2.5, 2.2, 2.5]);

    cabin.setTransformationMatrix(mat);

    self.addChild(cabin);
  }

  function placeBar(position) {
    var bar = new Box();
    bar.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, position);
    mat4.scale(mat, mat, [0.1, 0.1, 1.75]);

    bar.setTransformationMatrix(mat);

    self.addChild(bar); 
  }

  function placeRoof() {
    var roof = new Box();
    roof.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0, 3.05]);
    mat4.scale(mat, mat, [2.5, 2.2, 0.1]);

    roof.setTransformationMatrix(mat);

    self.addChild(roof);

    var roofTop = new TrainRoof();
    roofTop.material = new TrainRoofMaterial();

    mat = mat4.create();
    mat4.translate(mat, mat, [0, 0, 3.08]);
    mat4.scale(mat, mat, [2.5, 2.5, 2.5]);

    roofTop.setTransformationMatrix(mat);

    self.addChild(roofTop);
  }

  function placeBackGuard() {
    var backGuard = new Box();
    backGuard.material = new TrainMaterial();

    var mat = mat4.create();
    mat4.translate(mat, mat, [0, 0.4, -0.925]);
    mat4.scale(mat, mat, [2.7, 2.2, 1.85]);

    backGuard.setTransformationMatrix(mat);

    self.addChild(backGuard); 
  }
}

inheritPrototype(TrainCabin, Container);

function TrainRoof() {
  Shape.call(this);

  var path = new Bezier(3);
  path.controlPoints = [
    [-0.6, 0, 0],
    [0, 0, 0.15],
    [0.6, 0, 0]
  ];

  var faceVertices = [
    [-0.5, 0, 0],
    [0.5, 0, 0],
    [0.5, 0, 0],
    [0.5, 0.05, 0],
    [0.5, 0.05, 0],
    [-0.5, 0.05, 0],
    [-0.5, 0.05, 0],
    [-0.5, 0, 0]
  ];

  var faceNormals = [
    [0, -1, 0],
    [0, -1, 0],
    [1, 0, 0],
    [1, 0, 0],
    [0, 1, 0],
    [0, 1, 0],
    [-1, 0, 0],
    [-1, 0, 0]
  ];

  /* TODO: close the endings */
  this.extrude({vertices: faceVertices, normals: faceNormals}, path, 0.1, false);

  this.createBuffers();
}

inheritPrototype(TrainRoof, Grid);
