var textures = {}
function initTextures() {
  var texIdx = 0;
  function initTexture(name, imgID) {
    textures[name] = gl.createTexture();
    textures[name].image = document.getElementById(imgID);
    textures[name].type = gl.TEXTURE_2D;
    gl.bindTexture(gl.TEXTURE_2D, textures[name]);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, textures[name].image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);
    textures[name].idx = texIdx++;
  }

  function initCubeMap(name, posXImg, negXImg, posYImg, negYImg, posZImg, negZImg) {
    var imgs = [
      [gl.TEXTURE_CUBE_MAP_POSITIVE_X, document.getElementById(posXImg)],
      [gl.TEXTURE_CUBE_MAP_NEGATIVE_X, document.getElementById(negXImg)],
      [gl.TEXTURE_CUBE_MAP_POSITIVE_Y, document.getElementById(posYImg)],
      [gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, document.getElementById(negYImg)],
      [gl.TEXTURE_CUBE_MAP_POSITIVE_Z, document.getElementById(posZImg)],
      [gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, document.getElementById(negZImg)]
    ];
    textures[name] = gl.createTexture();
    textures[name].type = gl.TEXTURE_CUBE_MAP;
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, textures[name]);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    
    for(var i = 0; i < imgs.length; i++)
      gl.texImage2D(imgs[i][0], 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, imgs[i][1]);
    
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);
    // gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    textures[name].idx = texIdx++;
  }

  initTexture('heightmap', 'heightmap');
  initCubeMap('skyBox', 'skyBoxPosX', 'skyBoxNegX', 'skyBoxPosY', 
              'skyBoxNegY', 'skyBoxPosZ', 'skyBoxNegZ');
  initTexture('grassDiffuse', 'grass-diffuse');
  initTexture('grassNormal', 'grass-normal');
  initTexture('waterDiffuse', 'water-diffuse');
  initTexture('waterNormal', 'water-normal');
  initTexture('railroadDiffuse', 'railroad-diffuse');
  initTexture('railroadNormal', 'railroad-normal');
  initTexture('barkDiffuse', 'bark-diffuse');
  initTexture('barkNormal', 'bark-normal');
  initTexture('treeDiffuse', 'tree-diffuse');
  initTexture('treeNormal', 'tree-normal');
}

function Material() {
  this.color = [1.0, 1.0, 1.0];
  this.ambientColor = [0.5, 0.5, 0.5];
  this.diffuseColor = [1.0, 1.0, 1.0];
  this.specularColor = [1.0, 1.0, 1.0];
  this.reflectiveness = 0.0;
  this.useWaterEffect = false;
  this.glossiness = 8;
  this.useRepetitionBreaking = false;
  this.isMetallic = false;
}

Material.prototype = {
  getColor: function() {
    return this.color;
  },

  getAmbientColor: function() {
    return this.ambientColor;
  },

  getDiffuseColor: function() {
    return this.diffuseColor;
  },

  getSpecularColor: function() {
    return this.specularColor;
  },

  getGlossiness: function() {
    return this.glossiness;
  },

  getReflectiveness: function() {
    return this.reflectiveness;
  },

  setUniforms: function(shaderProgram) {
    if(this.colorTex === undefined) {
      gl.uniform1i(shaderProgram.useColorTexUniform, false);
      gl.uniform3fv(shaderProgram.colorUniform, this.getColor());
    } else {
      gl.uniform1i(shaderProgram.useColorTexUniform, true);
      gl.uniform1i(shaderProgram.useRepetitionBreakingUniform, this.useRepetitionBreaking);
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(this.colorTex.type, this.colorTex);
      gl.uniform1i(shaderProgram.texColorUniform, 0);
    }

    if(this.normalTex === undefined) {
      gl.uniform1i(shaderProgram.useNormalMapUniform, false);
    } else {
      gl.uniform1i(shaderProgram.useNormalMapUniform, true);
      gl.activeTexture(gl.TEXTURE1);
      gl.bindTexture(this.normalTex.type, this.normalTex);
      gl.uniform1i(shaderProgram.texNormalUniform, 1); 
    }

    if(this.skyBoxTex === undefined){
      gl.uniform1i(shaderProgram.isSkyBoxUniform, false);
    } else {
      gl.uniform1i(shaderProgram.isSkyBoxUniform, true);
    }

    /* always passes the skybox for the environment mapping */
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(textures.skyBox.type, textures.skyBox);
    gl.uniform1i(shaderProgram.texSkyBoxUniform, 2); 

    gl.uniform1i(shaderProgram.useWaterEffectUniform, this.useWaterEffect);
    gl.uniform3fv(shaderProgram.ambientColorUniform, this.getAmbientColor());
    gl.uniform3fv(shaderProgram.diffuseColorUniform, this.getDiffuseColor());
    gl.uniform3fv(shaderProgram.specularColorUniform, this.getSpecularColor());
    gl.uniform1f(shaderProgram.reflectivenessUniform, this.getReflectiveness());
    gl.uniform1f(shaderProgram.glossinessUniform, this.getGlossiness());
    gl.uniform1f(shaderProgram.isMetallicUniform, this.isMetallic);
  }
};

function WaterMaterial() {
  Material.call(this);
  this.colorTex = textures.waterDiffuse;
  this.normalTex = textures.waterNormal;
  this.glossiness = 256;
  this.diffuseColor = [0.4,0.4,0.4];
  this.ambientColor = [0.2,0.2,0.2];
  this.useWaterEffect = true;
  this.reflectiveness = 0.6;
}

inheritPrototype(WaterMaterial, Material);

function SkyBoxMaterial() {
  Material.call(this);
  this.skyBoxTex = textures.skyBox;
  this.specularColor = [0.0, 0.0, 0.0];
}

inheritPrototype(SkyBoxMaterial, Material);

function MetallicMaterial() {
  Material.call(this);
  this.color = [0.7,0.7,0.7];
  this.specularColor = [1.0,1.0,1.0];
  this.diffuseColor = [0.6,0.6,0.6];
  this.glossiness = 16;
  this.reflectiveness = 0.5;
  this.isMetallic = true;
}

inheritPrototype(MetallicMaterial, Material);

function TrainMaterial() {
  MetallicMaterial.call(this);
  this.color = [0.8, 0.3, 0.3];
  this.specularColor = this.color;
}

inheritPrototype(TrainMaterial, MetallicMaterial);

function TrainRoofMaterial() {
  MetallicMaterial.call(this);
  this.color = [1.0, 1.0, 0.0];
}

inheritPrototype(TrainRoofMaterial, MetallicMaterial);

function MovingBarMaterial() {
  MetallicMaterial.call(this);
  this.color = [0.5, 0.5, 0.5];
}

inheritPrototype(MovingBarMaterial, MetallicMaterial);

function MateMaterial() {
  Material.call(this);
  this.specularColor = [0.0, 0.0, 0.0];
}

inheritPrototype(MateMaterial, Material);

function WheelMaterial() {
  MateMaterial.call(this);
  this.color = [0.0, 0.0, 0.0];
}

inheritPrototype(WheelMaterial, MateMaterial);

function GrassMaterial() {
  MateMaterial.call(this);
  this.colorTex = textures.grassDiffuse;
  this.useRepetitionBreaking = true;
  this.normalTex = textures.grassNormal;
  this.specularColor = [0.1, 0.1, 0.1];
}

inheritPrototype(GrassMaterial, MateMaterial);

function DirtMaterial() {
  MateMaterial.call(this);
  this.colorTex = textures.railroadDiffuse;
  this.normalTex = textures.railroadNormal;
}

inheritPrototype(DirtMaterial, MateMaterial);

function WoodMaterial() {
  MateMaterial.call(this);
  this.texClippingArea = [0.0, 0.0, 1.0, 0.5];
  this.colorTex = textures.barkDiffuse;
  this.normalTex = textures.barkNormal;
}

inheritPrototype(WoodMaterial, MateMaterial);

function LeavesMaterial() {
  MateMaterial.call(this);
  this.texClippingArea = [0.0, 0.5, 1.0, 0.5];
  this.colorTex = textures.treeDiffuse;
  this.normalTex = textures.treeNormal;
}

inheritPrototype(LeavesMaterial, MateMaterial);