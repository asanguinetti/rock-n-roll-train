if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    function F() {
    }

    F.prototype = o;
    return new F();
  };
}


function inheritPrototype(childObject, parentObject) {
  var copyOfParent = Object.create(parentObject.prototype);
  copyOfParent.constructor = childObject;
  childObject.prototype = copyOfParent;
}

/**
 *  Pseudo-Random Number Generator using a Linear Congruential Generator.
 *  @type {Object}
 */
PRNG = {
    x: (new Date()).valueOf(),
    seed: function(newSeed) {
      this.x = newSeed;
    },
    random: function() {
      this.x = (this.x * 1664525 + 1013904223) & 0x7FFFFFFF;
      return this.x / 0x7FFFFFFF;
    },
    /** This one gives a distribution a little bit more Gaussian. */
    randSum: function() {
      var sum = 0;
      var n = 3;
      for(var i = 0; i < n; i++)
        sum += this.random();
      return sum / n;    
    }
};