function Drawable() {
  this.mMat = mat4.create();
  this.composedMat = mat4.create();
  this.parentMat = mat4.create();
}

Drawable.prototype = {
  /* Abstract method */
  draw: function(shaderProgram) {
    throw "not implemented";
  },

  animate: function(t) {
  },

  setTransformationMatrix: function(matrix) {
    mat4.copy(this.mMat, matrix);
    mat4.multiply(this.composedMat, this.parentMat, this.mMat);
  },

  setParentTransformationMatrix: function(matrix) {
    mat4.copy(this.parentMat, matrix);
    mat4.multiply(this.composedMat, this.parentMat, this.mMat);
  },

  getTransformationMatrix: function(matrix) {
    if(matrix === undefined)
      return mat4.clone(this.composedMat);
    return mat4.copy(matrix, this.composedMat);
  }
}

/** Container drawable. It does not draw anything by itself.
 *  It just contains other drawable pieces.
 */
function Container() {
  Drawable.call(this);
  this.children = [];
}

inheritPrototype(Container, Drawable);

Container.prototype.draw = function(shaderProgram) {
  /* the object is just a container, let's draw the children */
  for (var i = 0; i < this.children.length; i++) {
    this.children[i].draw(shaderProgram);
  }
}

Container.prototype.animate = function(t) {
  /* the object is just a container, let's animate the children */
  for (var i = 0; i < this.children.length; i++) {
    this.children[i].animate(t);
  } 
}

Container.prototype.setTransformationMatrix = function(matrix) {
  Drawable.prototype.setTransformationMatrix.call(this, matrix);
  for(var i = 0; i < this.children.length; i++) {
    this.children[i].setParentTransformationMatrix(this.composedMat);
  }
}

Container.prototype.setParentTransformationMatrix = function(matrix) {
  Drawable.prototype.setParentTransformationMatrix.call(this, matrix);
  for(var i = 0; i < this.children.length; i++) {
    this.children[i].setParentTransformationMatrix(this.composedMat);
  }
}

Container.prototype.addChild = function(child) {
  this.children.push(child);
  child.setParentTransformationMatrix(this.composedMat);
}


/** Drawable type that actually contains drawable data.
 *  
 */
function Shape() {
  Drawable.call(this);
  this.material = new Material();
  this.nMat = mat3.create();
}

inheritPrototype(Shape, Drawable);

Shape.prototype.extrude = function(cutInfo, curve, deltaU, closed) {
  if(closed === undefined)
    closed = true;

  cutVertices = cutInfo.vertices;
  cutNormals = cutInfo.normals;
  cutTexCoords = cutInfo.texCoords;

  this.positionData = [];
  this.normalData = [];
  if(cutTexCoords !== undefined)
    this.texCoordsData = [];
  this.tangentData = [];

  this.nx = cutVertices.length;
  this.ny = 0;

  var mat = mat4.create();

  /* when extruding along a closed curve it is better to exceed from 1 rather than
   * to fall short */
  var limit = 1;
  if(closed)
    limit += deltaU;

  var dist = 0;
  /* creates the positions' buffer */
  for(var u = 0; u < limit; u += deltaU) {
    var pos = curve.getPoint(u);
    
    /* calculates the coordinate system for the new face */
    var tg = curve.getTangent(u);
    if(u > 0)
      dist += vec3.length(vec3.subtract([], curve.getPoint(u-deltaU), pos));
    vec3.normalize(tg, tg);
    var up = [0, 0, 1];
    var left = [];
    vec3.cross(left, up, tg);
    vec3.normalize(left, left);
    vec3.cross(up, tg, left);

    /* builds a matrix to change to the new coordinate system */
    mat4.identity(mat);
    mat4.translate(mat, mat, pos);
    mat4.multiply(mat, mat, [].concat(
      left, [0],
      up, [0],
      tg, [0],
      [0, 0, 0, 1]
    ));

    /* gets the transformation matrix for the normals */
    var normalMat = mat3.create();
    mat3.normalFromMat4(normalMat, mat);

    for(var i = 0; i < cutVertices.length; i++) {
      var vec = [];
      vec = vec3.transformMat4(vec, cutVertices[i], mat);
      this.positionData = this.positionData.concat(vec);
      vec = vec3.transformMat3(vec, cutNormals[i], normalMat);
      this.normalData = this.normalData.concat(vec);
      if(cutTexCoords !== undefined) {
        vec = vec2.add([], cutTexCoords[i], [0, dist * 0.05]);
        this.texCoordsData = this.texCoordsData.concat(vec);
      }
      this.tangentData = this.tangentData.concat(tg);
    }
    this.ny++;
  }
},

Shape.prototype.generateIndexData = function() {
  throw "not implemented";
},

Shape.prototype.createBuffers = function() {
  this.generateIndexData();

  /* creates the normals buffer */
  this.normalsVBO = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalsVBO);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.normalData), gl.STATIC_DRAW);
  this.normalsVBO.itemSize = 3;
  this.normalsVBO.numItems = this.normalData.length / 3;

  /* creates the tangents buffer */
  if(this.tangentData !== undefined) {
    this.tangentsVBO = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.tangentsVBO);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.tangentData), 
                  gl.STATIC_DRAW);
    this.tangentsVBO.itemSize = 3;
    this.tangentsVBO.numItems = this.tangentData.length / 3;
  }

  /* creates the texture coordinates buffer */
  if(this.texCoordsData !== undefined) {
    this.texCoordsVBO = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordsVBO);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.texCoordsData), 
                  gl.STATIC_DRAW);
    this.texCoordsVBO.itemSize = 2;
    this.texCoordsVBO.numItems = this.texCoordsData.length / 2;
  }

  /* creates the positions buffer */
  this.positionsVBO = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, this.positionsVBO);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.positionData), 
                gl.STATIC_DRAW);
  this.positionsVBO.itemSize = 3;
  this.positionsVBO.numItems = this.positionData.length / 3;

  /* creates the index buffer */
  this.indicesEBO = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indicesEBO);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, 
                new Uint16Array(this.indexData), gl.STATIC_DRAW);
  this.indicesEBO.itemSize = 1;
  this.indicesEBO.numItems = this.indexData.length;
},

Shape.prototype.draw = function(shaderProgram) {
  /* passes the model matrix to the shader program */
  gl.uniformMatrix4fv(shaderProgram.mMatrixUniform, false, this.composedMat);

  /* gets the normal matrix from the model view matrix */
  mat3.normalFromMat4(this.nMat, this.composedMat);

  /* passes the normal matrix to the shader program */
  gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, this.nMat);

  /* passes the material uniforms to the shader program */
  this.material.setUniforms(shaderProgram);

  /* passes the positions buffer to the shader program */
  gl.bindBuffer(gl.ARRAY_BUFFER, this.positionsVBO);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 
                         this.positionsVBO.itemSize,
                         gl.FLOAT, false, 0, 0);

  /* passes the texture coordinates buffer to the shader program */
  if(this.texCoordsVBO !== undefined) {
    gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordsVBO);
    gl.vertexAttribPointer(shaderProgram.texCoordAttribute,
                           this.texCoordsVBO.itemSize,
                           gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shaderProgram.texCoordAttribute);
  } else {
    gl.disableVertexAttribArray(shaderProgram.texCoordAttribute);
  }

  /* passes the normals buffer to the shader program */
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalsVBO);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute,
                         this.normalsVBO.itemSize,
                         gl.FLOAT, false, 0, 0);

  /* passes the tangents buffer to the shader program */
  if(this.tangentsVBO !== undefined) {
    gl.bindBuffer(gl.ARRAY_BUFFER, this.tangentsVBO);
    gl.vertexAttribPointer(shaderProgram.vertexTangentAttribute,
                           this.tangentsVBO.itemSize,
                           gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shaderProgram.vertexTangentAttribute);
  } else {
    gl.disableVertexAttribArray(shaderProgram.vertexTangentAttribute);
  }

  /* passes the index buffer to the shader program */
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indicesEBO);
  gl.drawElements(gl.TRIANGLES, this.indicesEBO.numItems, 
                  gl.UNSIGNED_SHORT, 0);
}


/**
 *  Creates a grid of w by h, with nx and ny vertices on each
 *  dimension.
 *  @param {number} w Width of the grid.
 *  @param {number} h Height of the grid.
 *  @param {number} nx Number of vertices in the horizontal dimension.
 *  @param {number} ny Number of vertices in the vertical dimension.
 */
function Grid(w, h, nx, ny)
{
  Shape.call(this);
  this.nx = nx;
  this.ny = ny;
  /* creates the positions' buffer */
  this.positionData = [];
  this.texCoordsData = [];
  this.normalData = [];
  this.tangentData = [];
  for (var i = 0; i < ny; i++) {
    for (var j = 0; j < nx; j++) {
      this.positionData.push(-w/2 + w*j/(nx - 1));
      this.positionData.push(-h/2 + h*i/(ny - 1));
      this.positionData.push(0);
      this.texCoordsData.push(-w/2 + w*j/(nx - 1));
      this.texCoordsData.push(-h/2 + h*i/(ny - 1));
      this.normalData.push(0);
      this.normalData.push(0);
      this.normalData.push(1);
      this.tangentData = this.tangentData.concat([1, 0, 0]);
    }
  }

  this.createBuffers();
}

inheritPrototype(Grid, Shape);

Grid.prototype.generateIndexData = function() {
  /* creates the indices' buffer */
  this.indexData = [];
  /* iterates over the grid forming two triangles for each cell */
  for (var i = 0; i < this.ny - 1; i++) {
    for (var j = 0; j < this.nx - 1; j++) {
      /* 1st triangle */
      this.indexData.push(j + i*this.nx);
      this.indexData.push(j+1 + i*this.nx);
      this.indexData.push(j+1 + (i+1)*this.nx);

      /* 2nd triangle */
      this.indexData.push(j + i*this.nx);
      this.indexData.push(j+1 + (i+1)*this.nx);
      this.indexData.push(j + (i+1)*this.nx);
    }
  }
}


/** Drawable cylinder shape of diameter 1 and height 1.
 *  
 *  @param {number} nx Number of vertices in the radial direction.
 *  @param {number} ny Number of vertices in the height direction.
 */
function Cylinder(nx, ny, material) {
  Shape.call(this);
  this.height = 1;
  this.radius = 0.5;
  this.nx = nx;
  this.ny = ny;
  this.material = material;

  /* creates a cross-section like this:
   *   --
   *    |
   *    |
   *   -- 
   * */
  revSurf = [];
  revSurf.push([0, this.height/2, 0, 0, 1, 0, 1, 0, 0]);
  revSurf.push([this.radius, this.height/2, 0, 0, 1, 0, 1, 0, 0]);
  for (i = 0; i < nx - 4; i++) {
    revSurf.push([this.radius, -i*this.height/(nx-5) + this.height/2, 0, 1, 0, 0, 0, 1, 0]);
  }
  revSurf.push([this.radius, -this.height/2, 0, 0, -1, 0, -1, 0, 0]);
  revSurf.push([0, -this.height/2, 0, 0, -1, 0, -1, 0, 0]);

  /* revolves the cross-section around the primary axis */
  var revMat = mat4.create();
  this.positionData = [];
  this.normalData = [];
  this.tangentData = [];
  this.texCoordsData = [];
  for (j = 0; j < ny; j++){
    for (i = 0; i < nx; i++) {
      var pos = vec3.create();
      var normal = vec3.create();
      var tangent = vec3.create();
      
      vec3.transformMat4(pos, revSurf[i].slice(0,3), revMat);

      /* the transformation is just a rotation, so we don't need a special matrix
       * for the normals */
      vec3.transformMat4(normal, revSurf[i].slice(3,6), revMat);
      vec3.transformMat4(tangent, revSurf[i].slice(6,9), revMat);

      this.positionData.push(pos[0]);
      this.positionData.push(pos[1]);
      this.positionData.push(pos[2]);
      this.normalData.push(normal[0]);
      this.normalData.push(normal[1]);
      this.normalData.push(normal[2]);
      this.tangentData.push(tangent[0]);
      this.tangentData.push(tangent[1]);
      this.tangentData.push(tangent[2]);
      this.texCoordsData = this.texCoordsData.concat([j/(ny-1), pos[1]+this.height/2]);
    }
    mat4.rotateY(revMat, revMat, Math.PI * 2.0 / (ny-1) );
  }

  this.createBuffers();
};

inheritPrototype(Cylinder, Grid);


/** Landscape created like a grid which Z value for each vertex is taken
 *  from a heightmap on an image element.
 *  
 *  @param {number} nx Number of vertices on the X dimension.
 *  @param {number} ny Number of vertices on the Y dimension.
 *  @param {element} heightmap Image element containing the heightmap.
 */
function Landscape(nx, ny, heightmap) {
  Shape.call(this);
  var canvas = document.createElement('canvas');
  canvas.width = heightmap.width;
  canvas.height = heightmap.height;
  var auxCtx = canvas.getContext('2d');
  auxCtx.drawImage(heightmap, 0, 0, heightmap.width, 
                   heightmap.height);
  this.data = auxCtx.getImageData(0, 0, heightmap.width, 
                                  heightmap.height).data;

  this.width = heightmap.width;
  this.height = heightmap.height;
  this.nx = nx;
  this.ny = ny;
  this.material = new GrassMaterial();

  /* creates the positions and normals buffers */
  this.positionData = [];
  this.normalData = [];
  this.tangentData = [];
  this.texCoordsData = [];
  for (var i = 0; i < ny; i++) {
    /* splits the concatenation into smaller arrays for performance reasons */
    for (var j = 0; j < nx; j++) {
      var x = -this.width/2 + this.width*j/nx;
      var y = -this.height/2 + this.height*i/ny;
      /* samples the greyscale value from the heightmap */
      var z = this.getValue(x, y);

      this.positionData.push(x);
      this.positionData.push(y);
      this.positionData.push(z);

      this.texCoordsData.push(x/16);
      this.texCoordsData.push(y/16);

      var normal = this.getNormal(x, y);
      this.normalData.push(normal[0]);
      this.normalData.push(normal[1]);
      this.normalData.push(normal[2]);

      this.tangentData.push(1);
      this.tangentData.push(0);
      this.tangentData.push(0);
    }
  }

  this.createBuffers();
}

inheritPrototype(Landscape, Grid);

Landscape.prototype.getHeight = function(x, y) {
  var mat = mat4.create();
  this.getTransformationMatrix(mat);
  mat4.invert(mat, mat);
  
  var pos = [x, y, 0];
  vec3.transformMat4(pos, pos, mat);

  z = this.getValue(pos[0], pos[1]);

  var pos = [x, y, z];
  vec3.transformMat4(pos, pos, mat);

  return pos[2];
};

Landscape.prototype.getValue = function(x, y) {
  /* interpolates the coordinates on the heightmap */
  var u = Math.floor(x) + this.width/2;
  var v = this.height - Math.floor(y) - this.height/2;

  return (this.data[v*(this.width*4) + u*4] - 64) * 0.2;
};

Landscape.prototype.getNormal = function(x, y) {
  h = 6;
  x1 = Math.max(x-h, -this.width/2);
  x2 = Math.min(x+h, this.width/2);
  y1 = Math.max(y-h, -this.height/2);
  y2 = Math.min(y+h, this.height/2);
  derivX = (this.getValue(x2, y) - this.getValue(x1, y))/(x2 - x1);
  derivY = (this.getValue(x, y2) - this.getValue(x, y1))/(y2 - y1);
  var normal = [-derivX, -derivY, 1];
  return vec3.normalize(normal, normal);
};

function Box() {
  Shape.call(this);

  /* creates the positions' buffer */
  var faceVertices = [
    [-0.5, 0.5, 0.5],
    [-0.5, -0.5, 0.5],
    [0.5, -0.5, 0.5],
    [0.5, 0.5, 0.5]
  ];
  var faceNormals = [
    [0, 0, 1],
    [0, 0, 1],
    [0, 0, 1],
    [0, 0, 1]
  ];

  this.positionData = [];
  this.normalData = [];

  /* adds the side faces */
  var rotMat = mat4.create();
  mat4.rotateY(rotMat, rotMat, Math.PI/2);
  for (var j = 0; j < 4; j++) {
    for (var i = 0; i < faceVertices.length; i++ ) {
      this.positionData = this.positionData.concat(faceVertices[i]);
      vec3.transformMat4(faceVertices[i], faceVertices[i], rotMat);
      this.normalData = this.normalData.concat(faceNormals[i]);
      vec3.transformMat4(faceNormals[i], faceNormals[i], rotMat);
    }
  }

  /* adds the bottom */
  var rotMat = mat4.create();
  mat4.rotateX(rotMat, rotMat, Math.PI/2);
  for (var i = 0; i < faceVertices.length; i++ ) {
    vec3.transformMat4(faceVertices[i], faceVertices[i], rotMat);
    this.positionData = this.positionData.concat(faceVertices[i]);
    vec3.transformMat4(faceNormals[i], faceNormals[i], rotMat);
    this.normalData = this.normalData.concat(faceNormals[i]);
  }

  /* adds the top */
  var rotMat = mat4.create();
  mat4.rotateX(rotMat, rotMat, -Math.PI);
  for (var i = 0; i < faceVertices.length; i++ ) {
    vec3.transformMat4(faceVertices[i], faceVertices[i], rotMat);
    this.positionData = this.positionData.concat(faceVertices[i]);
    vec3.transformMat4(faceNormals[i], faceNormals[i], rotMat);
    this.normalData = this.normalData.concat(faceNormals[i]);
  }

  this.createBuffers();
}

inheritPrototype(Box, Shape);

Box.prototype.generateIndexData = function() {
  /* creates the indices' buffer */
  this.indexData = [];

  /* iterates over the faces drawing two triangles for each one */
  for (var i = 0; i < this.positionData.length / 3; i += 4) {
    /* first triangle */
    this.indexData.push(i);
    this.indexData.push(i + 1);
    this.indexData.push(i + 2);

    /* second triangle */
    this.indexData.push(i + 2);
    this.indexData.push(i + 3);
    this.indexData.push(i);
  }
}

function Railroad(circuit) {
  Container.call(this);

  this.circuit = circuit;

  var railroadBase = new RailroadBase(circuit);

  var rail1 = new Rail(circuit, 1.5);
  var rail2 = new Rail(circuit, -1.5);

  var mat = mat4.create();
  mat4.translate(mat, mat, [0, 0, 1.75]);
  rail1.setTransformationMatrix(mat);
  rail2.setTransformationMatrix(mat);

  this.addChild(railroadBase);
  this.addChild(rail1);
  this.addChild(rail2);
};

inheritPrototype(Railroad, Container);

Railroad.prototype.getCircuit = function() {
  var transformedCircuit = this.circuit.clone();
  var mat = mat4.create();
  this.getTransformationMatrix(mat);
  for(var i = 0; i < transformedCircuit.controlPoints.length; i++) {
    vec3.transformMat4(transformedCircuit.controlPoints[i],
                       transformedCircuit.controlPoints[i], mat);
  }
  return transformedCircuit;
}

function RailroadBase(circuit) {
  Shape.call(this);
  this.material = new DirtMaterial();

  var cut = new Bezier(5);
  cut.controlPoints = [
    [17,0,0],
    [12,2,0],
    [13,5,0],
    [10,5,0],
    [5,5,0],
    [-5,5,0],
    [-10,5,0],
    [-13,5,0],
    [-12,2,0],
    [-17,0,0]
  ];

  /* scale the cut a little bit */
  var cutMat = mat4.create();
  mat4.scale(cutMat, cutMat, [0.5, 0.25, 1.0]);

  for (i in cut.controlPoints)
    vec3.transformMat4(cut.controlPoints[i], cut.controlPoints[i], cutMat);

  var faceVertices = [];
  var faceNormals = [];
  var faceCoords = [];
  var dist = 0;
  var deltaU = 0.025;
  for (var u = 0; u <= 1.0; u += deltaU){
    var point = cut.getPoint(u);
    faceVertices.push(point);
    var tg = cut.getTangent(u);
    if(u > 0)
      dist += vec3.length(vec3.subtract([], cut.getPoint(u-deltaU), point));
    vec3.normalize(tg, tg);
    var left = [0, 0, 1]
    var normal = []; 
    normal = vec3.cross(normal, tg, left);
    faceNormals.push(normal);
    faceCoords.push([dist, 0]);
  }

  /* normalizes the texture coordinates to the range [0,1] */
  for(var i = 0; i < faceCoords.length; i++){
    faceCoords[i][0] = faceCoords[i][0] / faceCoords[faceCoords.length - 1][0];
  }

  cutInfo = {
    vertices: faceVertices,
    normals: faceNormals,
    texCoords: faceCoords,
  }
  this.extrude(cutInfo, circuit, 0.01);

  this.createBuffers();
}

inheritPrototype(RailroadBase, Grid);

function Rail(circuit, offset) {
  Shape.call(this);
  this.material = new MetallicMaterial();

  var faceVertices = [
    [0.5, -0.5, 0],
    [0.5, 0.5, 0],
    [0.5, 0.5, 0],
    [-0.5, 0.5, 0],
    [-0.5, 0.5, 0],
    [-0.5, -0.5, 0]
  ];
  var faceNormals = [
    [1, 0, 0],
    [1, 0, 0],
    [0, 1, 0],
    [0, 1, 0],
    [-1, 0, 0],
    [-1, 0, 0]
  ];

  /* creates a transformation matrix for the offset */
  var mat = mat4.create();
  mat4.translate(mat, mat, [offset, 0, 0]);

  /* applies the transformation to all the vertices */
  for(var i = 0; i < faceVertices.length; i++)
    vec3.transformMat4(faceVertices[i], faceVertices[i], mat);

  this.extrude({vertices: faceVertices, normals: faceNormals}, circuit, 0.01);

  this.createBuffers();
}

inheritPrototype(Rail, Grid);


function Tree(nx, ny) {
  Container.call(this);
  
  var trunk = new Cylinder(6, 8, new WoodMaterial());
  var mat = mat4.create();
  var rad = 0.3 + (PRNG.randSum() - 0.5) * 0.2;
  var height = 3.5 + (PRNG.randSum() - 0.5);
  mat4.translate(mat, mat, [0, 0, 1]);
  mat4.scale(mat, mat, [rad, rad, height]);
  mat4.rotateX(mat, mat, Math.PI/2);
  trunk.setTransformationMatrix(mat);

  var treeHead = new TreeHead(32, 32);
  var mat2 = mat4.create();
  mat4.translate(mat2, mat2, [0, 0, height]);
  mat4.scale(mat2, mat2, [
    4 + (PRNG.randSum() - 0.5) * 4.0, 
    4 + (PRNG.randSum() - 0.5) * 4.0, 
    4 + (PRNG.randSum() - 0.5) * 4.0
  ]);
  mat4.rotateX(mat2, mat2, Math.PI/2);
  treeHead.setTransformationMatrix(mat2);  

  this.addChild(trunk);
  this.addChild(treeHead);
};

inheritPrototype(Tree, Container);

function TreeHead(nx, ny) {
  Shape.call(this);
  this.material = new LeavesMaterial();
  this.nx = nx;
  this.ny = ny;
  var self = this;

  /* revolves the cross-section around the primary axis */
  this.positionData = [];
  this.normalData = [];
  this.tangentData = [];
  this.texCoordsData = [];

  /* generates an array of cross section cuts along the XZ plane. */
  var cuts = generateCuts();

  var dists = [];
  var arcs = [];
  for (j = 0; j < this.ny; j++){
    var dist = 0;
    for (i = 0; i < this.nx; i++) {
      var u = j/(this.ny - 1);

      /* gets each point along the coaxial axial curves */
      var pos = cuts[i].getPoint(u);
      this.positionData.push(pos[0]);
      this.positionData.push(pos[1]);
      this.positionData.push(pos[2]);

      var tg = cuts[i].getTangent(u);
      vec3.normalize(tg, tg);
      var p1, p2;
      if(i == 0)
        p1 = cuts[i].getPoint(u + 0.5);
      else
        p1 = cuts[i-1].getPoint(u);

      if(i == this.nx - 1)
        p2 = cuts[i].getPoint(u + 0.5);
      else
        p2 = cuts[i+1].getPoint(u);
      var right = vec3.subtract([], p2, p1);
      vec3.normalize(right, right);
      var normal = vec3.cross([], right, tg);
      this.normalData.push(normal[0]);
      this.normalData.push(normal[1]);
      this.normalData.push(normal[2]);
      this.tangentData.push(tg[0]);
      this.tangentData.push(tg[1]);
      this.tangentData.push(tg[2]);
      
      if(i > 0)
        dist += vec3.length(vec3.subtract([], cuts[i-1].getPoint(u), pos));

      dists.push(dist);
      arcs.push(j/this.ny);
    }
  }

  for(var j = 0; j < this.ny; j++){
    for(var i = 0; i < this.nx; i++){
      var dist = dists[j*this.nx + i]/dists[j*this.nx + (this.ny-1)];
      var arc = arcs[j*this.nx + i];
      this.texCoordsData.push(arc*2);
      this.texCoordsData.push(-dist*2);
    }
  }

  this.createBuffers();

  function generateCut() {
    var curve = new Bspline(4);
    curve.controlPoints = [
      /* starting point repeated k - 1 times to ensure interpolation */
      [0, 0.5, 0],
      [0, 0.5, 0],
      [0, 0.5, 0],

      /* makes sure the tangent at the beginning is in the horizontal plane */
      [0.1, 0.5, 0]
    ];

    for(var y = 0.4; y > -0.4; y -= (0.5 * 0.1))
      curve.controlPoints.push([0.3 + (PRNG.randSum() - 0.5) * 0.5, y, 0]);

    curve.controlPoints = curve.controlPoints.concat([
      /* makes sure the tangent at the ending is in the horizontal plane */
      [0.1, -0.5, 0],

      /* ending point repeated k - 1 times to ensure interpolation */
      [0, -0.5, 0],
      [0, -0.5, 0],
      [0, -0.5, 0]
    ]);

    revSurf = [];
    for(i = 0; i < self.nx; i++)
    {
      var u = i/self.nx;
      var point = curve.getPoint(u);
      revSurf.push(point);
    }

    return revSurf;
  }

  function generateCuts() {
    var cuts = [];
    var revMat = mat4.create();
    var cutsNum = 4;
    for(var i = 0; i < cutsNum; i++) {
      /* generates a cross section cut along the XY plane */
      var cut = generateCut();

      /* rotates the cross section */
      for(var j = 0; j < cut.length; j++) {
        vec3.transformMat4(cut[j], cut[j], revMat);
      }
      cuts.push(cut);

      /* increments the rotation */
      mat4.rotateY(revMat, revMat, Math.PI / 2.0 );
    }

    var k = 3;
    /* repeat the first k - 1 elements to keep the continuity properties at the end */
    for(var i = 0; i < (k - 1); i++)
      cuts.push(cuts[i]);

    /* makes an array of Bsplines whose control points are going to be the i-th
     * element of each cut from those generated above */
    var curves = [];
    for(var i = 0; i < cuts[0].length; i++) {
      curves.push(new Bspline(k));
      curves[i].controlPoints = [];
      for(var j = 0; j < cuts.length; j++) {
        curves[i].controlPoints.push(cuts[j][i]);
      }
    }
    return curves;
  }
};

inheritPrototype(TreeHead, Grid);

function SkyBox() {
  Box.call(this);
  this.material = new SkyBoxMaterial();
}

inheritPrototype(SkyBox, Box);

SkyBox.prototype.draw = function(shaderProgram) {
  gl.cullFace(gl.FRONT);
  gl.depthFunc(gl.LEQUAL);
  Box.prototype.draw.call(this, shaderProgram);
  gl.cullFace(gl.BACK);
  gl.depthFunc(gl.LESS);
}

function Water(w, h, nx, ny) {
  Grid.call(this, w, h, nx, ny);
  this.material = new WaterMaterial();
}

inheritPrototype(Water, Grid);

Water.prototype.draw = function(shaderProgram) {
  gl.enable(gl.BLEND);
  gl.depthMask(false);
  Grid.prototype.draw.call(this, shaderProgram);
  gl.depthMask(true);
  gl.disable(gl.BLEND);
}